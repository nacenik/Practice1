import com.epam.rd.java.basic.practice1.*;

public class Demo {
  private static final String TASK = " Task";
  private static final String LINE = "______________";
  
  public static void main(String[] args) {
    System.out.println(1 + TASK);
    Part1.main(args);
    System.out.println(LINE);
    System.out.println(2 + TASK);
    Part2.main(new String[]{"2", "4", "50"});
    System.out.println(LINE);
    System.out.println(3 + TASK);
    Part3.main(new String[]{"Hello", "World", "!   "});
    System.out.println(LINE);
    System.out.println(4 + TASK);
    Part4.main(new String[]{"100", "15"});
    System.out.println(LINE);
    System.out.println(5 + TASK);
    Part5.main(new String[]{"-4", "2", "-154","245", "1"});
    System.out.println(LINE);
    System.out.println(6 + TASK);
    Part6.main(new String[]{"100"});
    System.out.println(LINE);
    System.out.println(7 + TASK);
    Part7.main(new String[0]);
    System.out.println(LINE);
  }
}
