package com.epam.rd.java.basic.practice1;

import java.util.stream.IntStream;

public class Part6 {
  public static void main(String[] args) {
    int num = Integer.parseInt(args[0]);
    if (num > 1) {
      for (int i = 2; i < num; i++) {
        if (!isPrime(i)) {
          System.out.println(i);
        }
      }
    }
  }
  
  private static boolean isPrime(final int number) {
    for (int i = 2; i < number / 2 + 1; i++) {
      if (number % i == 0){
        return true;
      }
    }
    return false;
  }
}
