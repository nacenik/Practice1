package com.epam.rd.java.basic.practice1;

public class Part2 {
  private static final String error = "you don't have enough numbers";
  
  public static void main(String[] args) {
    if (args.length > 1) {
      int sum = doAddition(args);
      System.out.println(sum);
    } else {
      System.out.println(error);
    }
  }
  
  private static int doAddition(String[] nums) {
    int sum = 0;
    for (int i = 0; i < nums.length; i++) {
      sum = Integer.parseInt(nums[i]);
    }
    return sum;
  }
}
