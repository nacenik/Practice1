package com.epam.rd.java.basic.practice1;

public class Part7 {
  
  private static final int ALPHABET_COUNT = 26;
  
  public static void main(String[] args) {
    System.out.println(getColumnByWords("A"));
    System.out.println(getColumnByNumbers(1));
    System.out.println(getRightColumn("A"));
  
    System.out.println(getColumnByWords("B"));
    System.out.println(getColumnByNumbers(2));
    System.out.println(getRightColumn("B"));
  
    System.out.println(getColumnByWords("Z"));
    System.out.println(getColumnByNumbers(26));
    System.out.println(getRightColumn("Z"));
  
    System.out.println(getColumnByWords("AA"));
    System.out.println(getColumnByNumbers(27));
    System.out.println(getRightColumn("AA"));
  
    System.out.println(getColumnByWords("AZ"));
    System.out.println(getColumnByNumbers(52));
    System.out.println(getRightColumn("AZ"));
  
    System.out.println(getColumnByWords("BA"));
    System.out.println(getColumnByNumbers(53));
    System.out.println(getRightColumn("BA"));
  
    System.out.println(getColumnByWords("ZZ"));
    System.out.println(getColumnByNumbers(702));
    System.out.println(getRightColumn("ZZ"));
  
    System.out.println(getColumnByWords("AAA"));
    System.out.println(getColumnByNumbers(703));
    System.out.println(getRightColumn("AAA"));
  }
  
  private static int getColumnByWords(String words) {
    if (!verifyUpperCase(words)) {
      return -1;
    }
    int pos = 0;
    for (int i = 0; i < words.length(); i++) {
      pos *= ALPHABET_COUNT;
      pos += (words.charAt(i) - 'A' + 1);
    }
    return pos;
  }
  
  private static String getColumnByNumbers(int num) {
    StringBuilder stringBuilder = new StringBuilder();
    while (num > 0) {
      int prev = num % 26;
      
      if (prev == 0) {
        stringBuilder.append("Z");
        num = (num / 26) - 1;
      }
      else
      {
        stringBuilder.append((char)((prev - 1) + 'A'));
        num = num / 26;
      }
    }
    return stringBuilder.reverse().toString();
  }
  
  private static String getRightColumn(String words) {
    int next = getColumnByWords(words) + 1;
    return getColumnByNumbers(next);
  }
  
  private static boolean verifyUpperCase(String words) {
    return words.matches("([A-Z])+");
  }
}
