package com.epam.rd.java.basic.practice1;

public class Part3 {
  
  public static void main(String[] args) {
    System.out.println(concatStrings(args));
  }
  
  private static String concatStrings(String[] args) {
    return String.join(" ", args).trim();
  }
}
