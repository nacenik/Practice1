package com.epam.rd.java.basic.practice1;

public class Part4 {
  
  public static void main(String[] args) {
    if (args.length == 2) {
      int first = Integer.parseInt(args[0]);
      int second = Integer.parseInt(args[1]);
      int divider = 1;
      for (int i = 2; i < Math.min(first, second); i++) {
        if (first % i == 0 && second % i == 0) {
          divider = i;
        }
      }
      System.out.println(divider);
    }
  }
}
