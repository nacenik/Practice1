package com.epam.rd.java.basic.practice1;

public class Part5 {
  public static void main(String[] args) {
    int sum = 0;
    for (String num : args) {
      int number = Integer.parseInt(num);
      if (number > 0) {
        sum += number;
      }
    }
    System.out.println(sum);
  }
}
